const
  Size = 20;
  MaxValueA = 9;
  MaxValueB = 10;

type
  Matrix = array [1..Size, 1..Size] of integer;
  Massiv = array [1..Size] of integer;

var
  i, j, CountA, CountB: integer;
  a, b, RezultsC: Matrix;
  Rezultsd: Massiv;


procedure CreateMass(var h: Matrix; MaxValue: integer);
var
  i, j: byte;
begin
  for i := 1 to Size do
    for j := 1 to Size do
      h[i, j] := random(2 * MaxValue) - MaxValue;    // ������� ��������� ��������� ������� � ������ ��������� 
end;


procedure PrintMass(h: Matrix);
var
  i, j: byte;
begin
  for i := 1 to Size do
  begin
    for j := 1 to Size do
      write(h[i, j]:3);
    writeln(' ');
  end;
end;

procedure GetSequence1(var a, b, h: Matrix);
var
  i, j: byte;
begin
  for i := 1 to Size do
    for j := 1 to Size do
      if a[i, j] > b[i, j] then h[i, j] := 1 else h[i, j] := 0;
end;

procedure PrintSequence1(h: Matrix);
var
  i, j: byte;
begin
  writeln('             c=');
  for i := 1 to Size do
  begin
    write('Dly ', i, ' stroki: ');
    for j := 1 to Size do
      write(RezultsC[i, j]:3);
    writeln(' ');
  end;
end;

procedure GetSequence2(a, b: Matrix);
var
  i, j: byte;
begin
  for i := 1 to Size do
  begin
    for j := 1 to Size do
    begin
      if a[i, j] < 0 then CountA := CountA + 1;            // ����������� �������� ���������� ������������� ��������� ������� �
      if b[i, j] < 0 then CountB := CountB + 1;            // ����������� �������� ���������� ������������� ��������� ������� �
    end;
    if CountA = CountB then RezultsD[i] := 1 else RezultsD[i] := 0;
  end;
end;

procedure  PrintSequence2(Rezults: Massiv);
var
  i, j: byte;
begin
  writeln('              d=');
  for i := 1 to Size do
  begin
    write('Dly ', i, ' stroku: ');
    write(Rezults[i]:3);
    writeln(' ');
  end;
end;

function GetMedialValue (_Matrix:matrix):real;
var
 i, j:integer;
 MedialValue:real;
begin
     for i:=1 to n do
         for j:=1 to n do
         if (i+j=n+1) then

           MedialValue:=MedialValue+_Matrix[i,j];

     MedialValue:=MedialValue/(n*3);
 GetMedialValue:=MedialValue;
end;

begin
  randomize;
  
  CreateMass(a, MaxValueA);
  CreateMass(b, MaxValueB);
  
  
  writeln('Masssiv A');
  PrintMass(a);
  
  writeln(' ');
  
  writeln('Masssiv b');
  PrintMass(b);
  
  writeln(' ');
  
  GetSequence1(a, b, RezultsC);
    
  PrintSequence1(RezultsC);
  
  writeln(' ');
  
  GetSequence2(a, b);
  
  writeln(' ');
  
  PrintSequence2(RezultsD);
  
  readln;
end.